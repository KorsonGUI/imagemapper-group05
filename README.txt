  ___ ___   _   ___  __  __ ___ 
 | _ \ __| /_\ |   \|  \/  | __|
 |   / _| / _ \| |) | |\/| | _| 
 |_|_\___/_/ \_\___/|_|  |_|___|
                                
Start via the main method, enter your preferred map height and width and choose an image to v=convert. If no special mapping is given, a standard mapping is used, visible in main. If you want to adjust the mapping to your image, use the choose mapping button and confirm the number of differentiations to take and then enter them. if a field is left blank '.' is used.