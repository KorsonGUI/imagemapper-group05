import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Map;

public class Mapper {

    private final int[][] pixels;
    private final Map<Integer, Character> mappings;
    private BufferedImage img;

    public Mapper(BufferedImage img, Map<Integer, Character> mappings) {
        if (img.getHeight() > 128 || img.getWidth() > 128) {
            throw new IllegalArgumentException();
        }
        this.mappings = mappings;
        this.img = img;
        this.pixels = convertTo2DUsingGetRGB(this.img);
    }


    private int[][] convertTo2DUsingGetRGB(BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();
        int[][] result = new int[height][width];
        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                result[row][col] = (new Color(image.getRGB(col, row)).getRed()
                        + new Color(image.getRGB(col, row)).getGreen()
                        + new Color(image.getRGB(col, row)).getBlue()) / 3;
            }
        }
        return result;
    }

    public String createMap() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(pixels[0].length).append('\n');
        stringBuilder.append(pixels.length).append('\n');
        for (int[] row : pixels) {
            for (int pixel : row) {
                stringBuilder.append(getOutPutChar(pixel));
            }
            stringBuilder.append('\n');
        }
        return stringBuilder.toString();
    }

    private char getOutPutChar(int pixel) {
        int scaled = (int) (pixel / 256.0 * mappings.size());
        return mappings.get(scaled);
    }


}
