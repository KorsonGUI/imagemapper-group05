import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class ImageMapperMain extends Application {

    private static final Map<Integer, Character> STANDARD_MAP;

    static {
        Map<Integer, Character> aMap = new TreeMap<>();
        aMap.put(0, '9');
        aMap.put(1, '6');
        aMap.put(2, '3');
        aMap.put(3, '1');
        aMap.put(4, '.');
        aMap.put(5, '.');
        aMap.put(6, '=');
        aMap.put(7, '#');
        aMap.put(8, '#');
        STANDARD_MAP = aMap;
    }

    private String buttonStyleSheet = "-fx-text-fill: white;" + "-fx-font-family: Arial;" + "-fx-background-color: rgb(16,66,128);" + "-fx-border-color: black";


    private Stage primaryStage;
    private String map;
    private TextField heightField;
    private TextField widthField;
    private Label imageNameLabel;
    private File imageFile = null;
    private Integer mappingSize = -1;
    private Map<Integer, TextField> mappingInputs = new HashMap<>();
    private Map<Integer, Character> currentMapping = new HashMap<>();
    private Stage dialog;
    private double height;
    private double width;


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        AnchorPane root = new AnchorPane();
        root.getChildren().add(getChooserButton());
        primaryStage.setScene(new Scene(root, 800, 800));
        primaryStage.show();
    }

    private Node getChooserButton() {
        VBox vb = new VBox();
        vb.getChildren().add(getStartButton());
        vb.getChildren().add(getFileAndMappingBox());
        vb.getChildren().add(getDisplayer());
        return vb;
    }

    private Node getFileAndMappingBox() {
        HBox hb = new HBox();
        hb.getChildren().add(getFileChooserButton());
        hb.getChildren().add(getMappingChooserButton());
        return hb;
    }

    private Node getMappingChooserButton() {
        Button btn = new Button();
        btn.setPrefSize(400, 300);
        btn.setStyle(buttonStyleSheet + "; -fx-font-size: 40");
        btn.setText("Choose Mapping");
        btn.setOnMouseClicked((MouseEvent e) -> {
            beginMappingSelection();
        });
        return btn;
    }


    private Node getFileChooserButton() {
        Button btn = new Button();
        btn.setPrefSize(400, 300);
        btn.setStyle(buttonStyleSheet + "; -fx-font-size: 40");
        btn.setText("Choose Image");
        btn.setOnMouseClicked((MouseEvent e) -> {
            FileChooser fj = new FileChooser();
            File img = fj.showOpenDialog(this.primaryStage);
            if (img != null) {
                this.imageNameLabel.setText(img.getName());
                this.imageFile = img;
            }
        });
        return btn;
    }

    private Node getStartButton() {
        Button btn = new Button();
        btn.setPrefSize(800, 300);
        btn.setStyle(buttonStyleSheet + "; -fx-font-size: 80");
        btn.setText("Convert Image");
        btn.setOnMouseClicked((MouseEvent e) -> {
            if (imageFile != null) {
                beginProcessing(imageFile);
            }
        });
        return btn;
    }

    private Node getDisplayer() {
        HBox hb = new HBox();
        hb.getChildren().add(getHeightFieldBox());
        hb.getChildren().add(getWidthFieldBox());
        hb.getChildren().add(getImageLabelBox());
        return hb;
    }

    private Node getImageLabelBox() {
        HBox hb = new HBox();
        this.imageNameLabel = new Label();
        hb.getChildren().add(imageNameLabel);
        hb.setAlignment(Pos.CENTER);
        hb.setPrefSize(500, 200);
        return hb;
    }

    private Node getHeightFieldBox() {
        HBox hb = new HBox();
        this.heightField = new TextField();
        heightField.setText("Height...");
        setOnHover(heightField);
        hb.setAlignment(Pos.CENTER);
        hb.setStyle("-fx-background-color: rgb(16,66,128);");
        hb.setPrefSize(200, 200);
        hb.getChildren().add(heightField);
        return hb;
    }

    private void setOnHover(TextField textField) {
        String displaytext = textField.getText();
        textField.setOnMouseEntered((MouseEvent ev) -> {
            if (textField.getText().equals(displaytext))
                textField.setText("");
        });
        textField.setOnMouseExited((MouseEvent ev) -> {
            if (textField.getText().equals("")) {
                textField.setText(displaytext);
            }
        });
    }

    private Node getWidthFieldBox() {
        HBox hb = new HBox();
        this.widthField = new TextField();
        widthField.setText("Width...");
        setOnHover(widthField);
        hb.setAlignment(Pos.CENTER);
        hb.setStyle("-fx-background-color: rgb(16,66,128);");
        hb.setPrefSize(200, 200);
        hb.getChildren().add(widthField);
        return hb;
    }

    private void beginProcessing(File img) {
        try {
            BufferedImage buffImg = ImageIO.read(img);
            if (buffImg.getWidth() > 128 || buffImg.getHeight() > 128) {
                Converter con = new Converter();
                try {
                    Dimension newDim = con.getScaledDimension(new Dimension(buffImg.getHeight(), buffImg.getWidth()),
                            new Dimension(getHeightscale(), getWidthscale()));
                    con.makeEven(newDim);
                    this.height = newDim.getHeight();
                    this.width = newDim.getWidth();
                    buffImg = con.resize(buffImg, (int) newDim.getHeight(), (int) newDim.getWidth());
                } catch (NumberFormatException ex) {
                    throwExceptionPopup(ex);
                }
            }
            Mapper mapper;
            if (currentMapping.isEmpty())
                mapper = new Mapper(buffImg, STANDARD_MAP);
            else
                mapper = new Mapper(buffImg, currentMapping);
            this.map = mapper.createMap();
            displayMap();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private int getWidthscale() {
        return Integer.parseInt(widthField.getText());
    }

    private int getHeightscale() {
        return Integer.parseInt(heightField.getText());
    }

    private void displayMap() {
        final Stage dialog = new Stage();
        AnchorPane ap = new AnchorPane();
        HBox hb = new HBox();
        TextArea text = new TextArea(map);
        text.setPrefColumnCount((int) width);
        text.setPrefRowCount((int) height);
        hb.getChildren().add(text);
        ap.getChildren().add(hb);
        dialog.setScene(new Scene(ap, 1280, 720));
        dialog.show();
    }

    public void throwExceptionPopup(Exception e) {
        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(primaryStage);
        VBox dialogVbox = new VBox(20);
        TextArea ta = new TextArea(new StringBuilder().append(e.toString()).append("\n\n\nStacktrace:\n").append(e.getStackTrace()).toString());
        ta.setPrefRowCount(20);
        dialogVbox.getChildren().add(ta);
        Scene dialogScene = new Scene(dialogVbox, 600, 300);
        dialog.setScene(dialogScene);
        dialog.show();

    }

    private void beginMappingSelection() {
        final Stage dialog = new Stage();
        this.dialog = dialog;
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(primaryStage);
        HBox selectorBox = new HBox();
        TextField tf = new TextField();
//        tf.setPrefWidth(100);
        Button confirm = getConfirmButton();
        confirm.setOnMouseClicked((MouseEvent e) -> {
            try {
                this.mappingSize = Integer.parseInt(tf.getText());
            } catch (NumberFormatException ex) {
                throwExceptionPopup(ex);
            }
            displayMappingPanel(dialog);
        });
        tf.setText("Enter differentiations...");
        setOnHover(tf);
        selectorBox.getChildren().add(tf);
        selectorBox.getChildren().add(confirm);
        selectorBox.setAlignment(Pos.CENTER);
        Scene selector = new Scene(selectorBox, 250, 30);
        dialog.setScene(selector);
        dialog.show();
    }

    public void displayMappingPanel(Stage dialog) {
        VBox dialogVbox = new VBox();
        dialogVbox.setPrefSize(250, mappingSize * 30 + 50);
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setPrefSize(250, mappingSize * 30 + 50);
        scrollPane.setContent(dialogVbox);
        for (int i = 0; i < mappingSize; i++) {
            dialogVbox.getChildren().add(getMappingSetterBox(i));
        }
        dialogVbox.getChildren().add(getConfirmButton());
        dialogVbox.setAlignment(Pos.TOP_CENTER);
        Scene dialogScene = new Scene(scrollPane, 250, mappingSize * 30 + 50);
        dialog.setScene(dialogScene);
        dialog.show();
    }

    private Button getConfirmButton() {
        Button btn = new Button();
        btn.setStyle(buttonStyleSheet);
        btn.setPrefSize(150, 30);
        btn.setText("Confirm");
        btn.setOnMouseClicked((MouseEvent e) -> {
            for (Map.Entry<Integer, TextField> entry : mappingInputs.entrySet()) {
                TextField tf = entry.getValue();
                Integer integer = entry.getKey();
                char val = '.';
                if (!tf.getText().equals("")) {
                    val = tf.getText().toCharArray()[0];
                }
                currentMapping.put(integer, val);
            }
            dialog.close();
        });
        return btn;
    }

    private Node getMappingSetterBox(int i) {
        HBox hb = new HBox();
        hb.setAlignment(Pos.CENTER);
        Label lab = new Label("Type for " + (i) + " - " + (i + 1) + "    ");
        TextField tf = new TextField();
        tf.setPrefWidth(50);
        mappingInputs.put(i, tf);
        hb.setPrefSize(150, 30);
        hb.getChildren().add(lab);
        hb.getChildren().add(tf);
        Label oldVal = new Label("  X   ");
        if (!currentMapping.isEmpty()) {
            oldVal.setText("Old val: " + currentMapping.get(i));
            tf.setText("" + currentMapping.get(i));
        } else {
            oldVal.setText("Old val:  " + STANDARD_MAP.get(i));
            tf.setText("" + STANDARD_MAP.get(i));
        }
        hb.getChildren().add(oldVal);
        return hb;
    }
}
